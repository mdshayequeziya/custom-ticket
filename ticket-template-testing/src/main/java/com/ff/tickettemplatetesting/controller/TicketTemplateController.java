package com.ff.tickettemplatetesting.controller;


import com.ff.tickettemplatetesting.entity.TicketTemplate;
import com.ff.tickettemplatetesting.repository.TemplateRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("templates")
@AllArgsConstructor
public class TicketTemplateController {

    private final TemplateRepository templateRepository;

    @PostMapping
    public TicketTemplate saveTemplate2(@RequestBody TicketTemplate ticketTemplate){

        ticketTemplate.setId("001");
        return templateRepository.save(ticketTemplate);

    }

}
