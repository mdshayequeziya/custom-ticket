package com.ff.tickettemplatetesting.controller;


import com.ff.tickettemplatetesting.entity.CreateTicketDto;
import com.ff.tickettemplatetesting.entity.Ticket;
import com.ff.tickettemplatetesting.entity.TicketTemplate;
import com.ff.tickettemplatetesting.repository.TemplateRepository;
import com.ff.tickettemplatetesting.repository.TicketRepository;
import io.micrometer.common.util.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("tickets")
@AllArgsConstructor
public class TicketController {

    private final TemplateRepository templateRepository;

    private final TicketRepository ticketRepository;

    @PostMapping
    public Ticket saveTickets(@RequestBody CreateTicketDto ticketDto){

        Map<String, Object> attributesValues = ticketDto.getAttributesValues();

        TicketTemplate template = templateRepository.findById(ticketDto.getTemplateId()).orElseThrow(RuntimeException::new);

        //Map<String, Object> attributesToSave = new HashMap<>();
        for(Object attribute : template.getFieldDetails()){

            Map<String, Object> attrMap = (Map<String, Object>) attribute;
            String attributeName = attrMap.get("label").toString();
            boolean check = (Boolean)attrMap.get("mandatory");
            if (attributesValues.containsKey(attributeName) && StringUtils.isNotBlank(attributesValues.get(attributeName).toString())) {
//                attributesToSave.put(attributeName, attributesValues.get(attributeName));
            }else if(check){
                throw new IllegalArgumentException("Attribute value not provided for: " + attributeName);
            }
        }


        attributesValues.put("status", "OPEN");
        attributesValues.put("customerStatus", "OPEN");

        Ticket ticket = new Ticket();
        ticket.setCreateEntity("pradeep","rpk@mail.com");
        ticket.setTicketTemplateId(ticketDto.getTemplateId());
        ticket.setAttributes(attributesValues);

        return ticketRepository.save(ticket);
    }
}
