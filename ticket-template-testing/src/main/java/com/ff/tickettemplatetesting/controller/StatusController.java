package com.ff.tickettemplatetesting.controller;


import com.ff.tickettemplatetesting.entity.StatusTransition;
import com.ff.tickettemplatetesting.entity.StatusTransitionDto;
import com.ff.tickettemplatetesting.repository.StatusTransitionRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@AllArgsConstructor
@Slf4j
public class StatusController {
    private final StatusTransitionRepository repository;
    @PostMapping("/status")
    public ResponseEntity saveTransitionTemplate(@RequestBody StatusTransitionDto dto){
       List<Object> statusList =  dto.getStatus();
       StatusTransition statusTransition = new StatusTransition();
       Map<String,Object> newStatus = statusTransition.getTransitionStatus();
        Map<String,String> statusManagement;
        statusManagement = statusTransition.getStatusManagement()==null ? new HashMap<>() : statusTransition.getStatusManagement();
       if(newStatus==null){
           newStatus= new HashMap<>();
           for (Object status : statusList){
               Map<String, Object> statusFromRequest = (Map<String, Object>) status;
               newStatus.put(statusFromRequest.get("currentStatus").toString(),status);
               statusManagement.put(statusFromRequest.get("currentStatus").toString(),statusFromRequest.get("currentStatus").toString());
           }
           statusTransition.setTransitionStatus(newStatus);
           statusTransition.setStatusManagement(statusManagement);
       }

       return ResponseEntity.ok(repository.save(statusTransition));






    }
}
