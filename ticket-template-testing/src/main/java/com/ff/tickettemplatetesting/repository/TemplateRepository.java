package com.ff.tickettemplatetesting.repository;

import com.ff.tickettemplatetesting.entity.TicketTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends MongoRepository<TicketTemplate, String> {

    TicketTemplate findByName(String name);
}
