package com.ff.tickettemplatetesting.repository;

import com.ff.tickettemplatetesting.entity.Attributes;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributeRepository extends MongoRepository<Attributes, String> {
}
