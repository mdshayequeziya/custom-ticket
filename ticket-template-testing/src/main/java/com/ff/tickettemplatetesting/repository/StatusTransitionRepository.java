package com.ff.tickettemplatetesting.repository;

import com.ff.tickettemplatetesting.entity.StatusTransition;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StatusTransitionRepository extends MongoRepository<StatusTransition, ObjectId> {
}
