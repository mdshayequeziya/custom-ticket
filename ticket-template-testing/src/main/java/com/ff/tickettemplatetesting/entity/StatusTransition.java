package com.ff.tickettemplatetesting.entity;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import java.util.Map;


@Data
@Document(collection = "Transition status")
public class StatusTransition {

   @Id
    private ObjectId id;

    Map<String,Object> transitionStatus ;
    Map<String,String> statusManagement;
}

