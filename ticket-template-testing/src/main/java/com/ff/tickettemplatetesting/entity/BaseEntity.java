package com.ff.tickettemplatetesting.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class BaseEntity {
    String createdOn;
    String lastUpdateOn;
    String createdByName;
    String createdByEmail;
    String modifiedByName;
    String modifiedByEmail;

    public void setCreateEntity(String userName,String userEmail){
        this.createdByEmail=userEmail;
        this.createdByName=userName;
        this.createdOn = getCurrentTime();

    }

    public void setModifiedEntity(String userName, String userEmail){
        this.modifiedByEmail = userEmail;
        this.modifiedByName = userName;
        this.lastUpdateOn = getCurrentTime();
    }
    private String getCurrentTime(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z");
        var date = ZonedDateTime.now(ZoneId.of("Asia/Kolkata"));
        return (date.format(dtf));
    }


}
