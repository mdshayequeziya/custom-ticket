package com.ff.tickettemplatetesting.entity;

import lombok.Data;

import java.util.Map;

@Data
public class CreateTicketDto {

    String templateId;
    Map<String, Object> attributesValues;
}
