package com.ff.tickettemplatetesting.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document
@Data
public class Ticket extends BaseEntity{

    @Id
    private String id;
    private String ticketTemplateId;
    private Map<String, Object> attributes;
    private Object stateTransition;
}
