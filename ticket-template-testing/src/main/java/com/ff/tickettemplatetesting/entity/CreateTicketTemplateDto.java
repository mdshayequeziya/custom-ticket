package com.ff.tickettemplatetesting.entity;

import lombok.Data;
import java.util.List;

@Data
public class CreateTicketTemplateDto {

    private String name;
    private String description;
    private List<Object> attributes;
}
