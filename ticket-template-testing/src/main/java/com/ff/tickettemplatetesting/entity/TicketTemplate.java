package com.ff.tickettemplatetesting.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

@Document("ticket_templates")
@Data
public class TicketTemplate {

    @Id
    private String id;
    private String name;
    private String description;
    private List<Map<String, Object>> fieldDetails;
}
