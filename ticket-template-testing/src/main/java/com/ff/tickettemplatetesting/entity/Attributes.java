package com.ff.tickettemplatetesting.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
public class Attributes {
    String type;

    String label;

    String placeholder;

    int maxLength;

    int minLength;

    String defaultValue;

    boolean isEnabled;

    boolean isMandatory;

    boolean isVisibleToInternalTeam;

    boolean isVisibleToCustomer;

}
