package com.ff.tickettemplatetesting.entity;

import lombok.Data;

import java.util.List;
@Data
public class StatusTransitionDto {
    List<Object> status;
}
