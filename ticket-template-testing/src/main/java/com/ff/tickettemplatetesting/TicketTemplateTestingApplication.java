package com.ff.tickettemplatetesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketTemplateTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketTemplateTestingApplication.class, args);
	}

}
